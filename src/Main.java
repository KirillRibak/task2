import entity.Musician;
import packer.ListPacker;
import packer.MapPacker;

import util.*;

import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        List<Musician> list = ListPacker.getList();


       StreamListMethods streamList = new StreamListMethods(list);

        StreamMapMethods streamMap = new StreamMapMethods(MapPacker.getMap());

        ListMethods listMethods = new ListMethods(list);

        MapMethods mapMethods = new MapMethods(MapPacker.getMap());

      //  listMethods.callAllMethods();
      //  mapMethods.callAllMethods();
       // streamList.callAllMethods();
       // streamMap.callAllMethods();

        //listMethods.doesEachElementContainACharter('o');
      //  mapMethods.returnSecondAndThirdElement();
        streamList.removeElementMoreThanX(50);
      //  streamMap.returnSecondAndThirdElement();


        //listMethods.returnThirdElement();
        //checked whether functions work

    }
}
