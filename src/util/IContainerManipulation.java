package util;

public interface IContainerManipulation {

    public void removeMaxElement();

    public void removeMinElement();

    public void removeElementMoreThanX(int x);

    public void removeElementLessThanX(int x);

    public int calculateTotalSumm();

    public boolean doesEachElementContainACharter(char j);

    public void sortByNameAndOrigin();

}
