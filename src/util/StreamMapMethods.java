package util;

import entity.Musician;

import java.util.Map;
import java.util.stream.Collectors;

public class StreamMapMethods implements IContainerManipulation {

    private Map<String, Musician> disk;


    public StreamMapMethods(Map<String, Musician> disk) {
        this.disk = disk;
    }

    @Override
    public void removeMaxElement() {
        disk.remove(disk.entrySet().stream().max((p1, p2) -> p1.getValue().getThebestFee()
                .compareTo(p2.getValue().getNuberOfSongs())).get());
    }

    @Override
    public void removeMinElement() {
        disk.remove(disk.entrySet().stream().min((p1, p2) -> p1.getValue().getThebestFee()
                .compareTo(p2.getValue().getNuberOfSongs())).get());
    }

    @Override
    public void removeElementMoreThanX(int x) {
        disk.entrySet().stream().filter((p) -> p.getValue()
                .getNuberOfSongs() > x)
                .collect(Collectors.toList());
    }

    @Override
    public void removeElementLessThanX(int x) {
        disk.entrySet().stream().filter((p) -> p.getValue()
                .getNuberOfSongs() < x)
                .collect(Collectors.toList());
    }

    @Override
    public int calculateTotalSumm() {
        System.out.println(disk.entrySet().stream().mapToInt((s) -> Integer.parseInt("" + s.getValue().getThebestFee())).sum());
        return 0;
    }

    @Override
    public boolean doesEachElementContainACharter(char j) {
        return disk.entrySet().stream().allMatch((s) -> s.getValue().getName().contains("" + j));
    }

    @Override
    public void sortByNameAndOrigin() {
        disk.entrySet().stream().sorted((o1, o2) -> o1.getValue().getName()
                .compareTo(o2.getValue().getName()) == 0 ?
                o1.getValue().getOrigin().compareTo(o2.getValue().getOrigin())
                : o1.getValue().getName().compareTo(o2.getValue().getName()))
                .collect(Collectors.toList());
    }


    public Map.Entry<String, Musician> returnThirdElement() {
        return disk.entrySet().stream().skip(2).findFirst().get();
    }

    public Map<String, Musician> returnSecondAndThirdElement() {
        System.out.println(disk.entrySet().stream().skip(1).limit(2)
                .collect(Collectors.toMap(t -> t.getKey(), v -> v.getValue())));
        return disk.entrySet().stream().skip(1).limit(2)
                .collect(Collectors.toMap(t -> t.getKey(), v -> v.getValue()));
    }


    public Map<String, Musician> returnElementByTemplate() {
        return disk.entrySet().stream().filter((s) -> s.getValue().getOrigin()
                .contains("Toronto")).collect(Collectors.toMap(t -> t.getKey(), v -> v.getValue()));
    }

    public void callAllMethods() {
        returnElementByTemplate();
        returnSecondAndThirdElement();
        returnThirdElement();
        doesEachElementContainACharter('i');
        sortByNameAndOrigin();
        calculateTotalSumm();
        removeMaxElement();
        removeMinElement();
        removeElementLessThanX(25);
        removeElementMoreThanX(43);
    }

}
